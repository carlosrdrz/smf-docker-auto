<?php
include('/app/Settings.php');

// Filename to import
$filename = getenv("SMF_DIR").'/dump.sql';

// Connect to MySQL server. Keep trying for 5 secs until the
// mysql container is up and ready.
for ($i = 0; $i < 5; $i++) {
  $con = mysqli_connect($db_server, $db_user, $db_passwd, $db_name);
  if (mysqli_connect_errno()) {
    echo 'Error connecting to MySQL server: ' . mysqli_connect_error();
    sleep(1);
  } else {
    break;
  }
}

// Check if the database is already loaded
$result = mysqli_query($con, "SHOW TABLES LIKE '".getenv("SMF_PREFIX")."attachments'");
$numTables = mysqli_num_rows($result);

if (!$numTables) {
  // Temporary variable, used to store current query
  $templine = '';
  // Read in entire file
  $lines = file($filename);

  // Loop through each line
  foreach ($lines as $line) {
    // Skip it if it's a comment
    if (substr($line, 0, 2) == '--' || $line == '')
        continue;

    // Add this line to the current segment
    $templine .= $line;

    // If it has a semicolon at the end, it's the end of the query
    if (substr(trim($line), -1, 1) == ';') {
        // Perform the query
        mysqli_query($con, $templine) or print('Error performing query \'<strong>' . $templine . '\': ' . mysql_error() . '<br /><br />');
        // Reset temp variable to empty
        $templine = '';
    }
  }
}
?>