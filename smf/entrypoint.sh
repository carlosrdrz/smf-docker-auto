#!/bin/bash
set -e
source $BITNAMI_PREFIX/bitnami-utils.sh

print_welcome_page

sudo chown -R $BITNAMI_APP_USER:$BITNAMI_APP_USER \
  $BITNAMI_APP_VOL_PREFIX/conf/ \
  $BITNAMI_APP_VOL_PREFIX/logs/ \
  /app || true

if [ ! "$(ls -A $BITNAMI_APP_VOL_PREFIX/conf)" ]; then
  generate_conf_files $BITNAMI_APP_DIR/etc
fi

if [ ! -f /app/Settings.php ]; then
  # Remove app data and copy smf files into it
  cp -R ${SMF_DIR_APP}/* /app/

  # Replace the default settings with the data provided by the user
  # via environment vars
  sed -i -e "s/SMFDBSERVER/${SMF_DB_SERVER}/gI" $SMF_DIR/Settings.php $SMF_DIR/dump.sql
  sed -i -e "s/SMFDBUSER/${SMF_DB_USER}/gI" $SMF_DIR/Settings.php $SMF_DIR/dump.sql
  sed -i -e "s/SMFDBPASSWORD/${SMF_DB_PASSWORD}/gI" $SMF_DIR/Settings.php $SMF_DIR/dump.sql
  sed -i -e "s/SMFDBNAME/${SMF_DB_NAME}/gI" $SMF_DIR/Settings.php $SMF_DIR/dump.sql
  sed -i -e "s/SMFDBPREFIX/${SMF_DB_PREFIX}/gI" $SMF_DIR/Settings.php $SMF_DIR/dump.sql
  sed -i -e "s/SMFCOMMUNITYNAME/${SMF_FORUM_NAME}/gI" $SMF_DIR/Settings.php $SMF_DIR/dump.sql
  sed -i -e "s/SMFCOMMUNITYURL/${SMF_FORUM_URL}/gI" $SMF_DIR/Settings.php $SMF_DIR/dump.sql
  sed -i -e "s/SMFADMINEMAIL@NODOMAIN.COM/${SMF_ADMIN_EMAIL}/gI" $SMF_DIR/Settings.php $SMF_DIR/dump.sql
  cp $SMF_DIR/Settings.php /app/

  # To generate the admin user and password we have to do some tricky stuff..
  # The password is hashed like this on the DB: sha1(strtolower($username) . $passwd)
  # So we need to change our admin username, and then change the hash associated with it
  # The admin username placeholder is SMFADMINUSERNAME and the password is SMFADMINPASSWORD
  # If you do the hashing process, you get cabe8fcc76fcd21fac38e9aa04ad30132d374d80, which
  # is the hash that we have to change on the dump file to set the admin password
  ADMIN_USERNAME_LOWERCASE=$(echo $SMF_ADMIN_USERNAME | tr '[A-Z]' '[a-z]')
  ADMIN_PASSWORD_HASH=$(echo -n "${ADMIN_USERNAME_LOWERCASE}${SMF_ADMIN_PASSWORD}" | sha1sum | awk '{print $1}')
  sed -i -e "s/SMFADMINUSERNAME/${SMF_ADMIN_USERNAME}/gI" $SMF_DIR/dump.sql
  sed -i -e "s/cabe8fcc76fcd21fac38e9aa04ad30132d374d80/${ADMIN_PASSWORD_HASH}/gI" $SMF_DIR/dump.sql

  # Remove install files
  rm -f /app/install*
fi

# Give access to files
chmod 755 /app

# Upload the database to our MySQL server
/opt/bitnami/php/bin/php $SMF_DIR/import_dump.php

exec "$@"