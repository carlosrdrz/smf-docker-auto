# Introduction

This project provides the necessary containers and files to deploy a configured
version of SMF using docker containers, both locally using Docker Compose and
on a Kubernetes cluster running on Google Compute Engine. 

## [MariaDB](https://github.com/bitnami/bitnami-docker-mariadb)

MariaDB will be used as the database server of choice and will be used by SMF
to store its database schema.

## SMF

This container is based on bitnami/php-fpm. The necessary files to build this
container can be found on the folder 'smf'.
It mainly does the following:
- Includes the latest version of SMF, which is download when building the
container
- Configures the SMF Setting file and the needed files permissions.
- Populates the db with the default configuration when is started to bypass
the installation wizard.

## Apache

This container is based on bitnami/apache. The necessary files to build this
container can be found on the folder 'apache'.
It includes a vhost file that you can be configured with environment variables.
It is useful in order to avoid mounting a volume and creating the vhost file.
With this container all is already configured when you bring up the app.

# Adapt to your needs
You can change the version of SMF that you will be using on the smf container's
Dockerfile. Just change the DOWNLOAD_URL environment variable to the one that
you prefer. You have an example of how to use an older version of SMF on the
Dockerfile.

Take a look at the docker-compose.yml file. You will find some environment
variables that you can change to adapt the installation to your needs. Mainly,
you should change the db and smf admin user/password. You can also change the
name of your forum and the URL associated with it. Just keep in mind that both
SMF_FORUM_URL and VHOST_HOSTNAME must point to the same URL.

Leave the value of PHP_FPM_CONTAINER_NAME as localhost, since the apache and
php-fpm containers are running on the same pod (and host).

# Deploying locally with Docker Compose

## Step 1: Install Docker Compose

Follow this guide for installing Docker Compose.

- [https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/)

## Step 2: Run Docker Compose

All is already configured for you, so you just have to type:

```bash
docker-compose up
```

and Docker Compose will bring up the app.

## Step 3: Start browsing your new forum

To access the `ServerName` we set up in our VirtualHost configuration, you may
need to create an entry in your `/etc/hosts` file to point `smf.example.com` to
`localhost`.

```bash
echo '127.0.0.1 smf.example.com' | sudo tee -a /etc/hosts
```

Navigate to [http://smf.example.com/](http://smf.example.com/) in your browser
to navigate to your new forum.

# Deploying on Kubernetes

## Set up your Google Container Engine environment

Use [these instructions](https://cloud.google.com/container-engine/docs/before-you-begin).

## Upload the images to Google Container Registry

The images provided in this repo (smf and apache) must be uploaded to Google
Container Registry in orderto be used by your Kubernetes cluster. To do so,
just do the following:

First, lets build and upload the apache image.
```console
$ pushd apache
$ docker build -t gcr.io/<your project id>/apache .
$ gcloud docker push gcr.io/<your project id>/apache
$ popd
```

Next, do the same with the smf image
```console
$ pushd smf
$ docker build -t gcr.io/<your project id>/smf .
$ gcloud docker push gcr.io/<your project id>/smf
$ popd
```

## Create a cluster

You will need a cluster of VMs running Kubernetes and Docker to run the
images that you just built. You can create a cluster with the following command:
```console
$ gcloud beta container clusters create smf
```

## Create the persistent disk for MariaDB

For this deployment you will use a persistent disk called mariadb-disk,
which will be used to store the database data. Doing so, if the mariadb pod is
restarted or destroyed, the database data will persist.

To create this disk, use the following commands
```console
$ gcloud compute disks create --size 200GB mariadb-disk
```

## Create MariaDB pod and service

Next, you will start your mariadb container. To do so, run:
```console
$ kubectl create -f mariadb-controller.yml
```

You can see your pod running using the following command:
```bash
$ kubectl get pods -l name=mariadb
NAME            READY     REASON    RESTARTS   AGE
mariadb-mmae9   1/1       Running   0          31s
```

Now, you need a kubernetes service to access the container you just created.
To do so, use the following command:

```bash
$ kubectl create -f mariadb-service.yml
```

See it running:

```bash
$ kubectl get services mariadb
NAME      LABELS         SELECTOR       IP(S)           PORT(S)
mariadb   name=mariadb   name=mariadb   10.99.253.149   3306/TCP
```

Doing so, MariaDB is already up and ready.

## Create SMF and Apache pod and service

In this case, both apache and php-fpm are deployed on a single pod. We need
these containers to run on the same pod in order to share the application files
between them, using a emptyDir volume.
Please see the last section of this document for more information about this.

Use the following commands:
```bash
$ kubectl create -f smf-controller.yml
$ kubectl create -f smf-service.yml
```

After that, your cluster will be up and ready. You can check the state of all
your pods and services again to see that all of them are up and ready:

```bash
$ kubectl get pods
NAME            READY     STATUS    RESTARTS   AGE
mariadb-i9hvw   1/1       Running   0          1m
smf-0yfjd       2/2       Running   0          1m

$ kubectl get services
NAME         LABELS                                    SELECTOR       IP(S)           PORT(S)
kubernetes   component=apiserver,provider=kubernetes   <none>         10.95.240.1     443/TCP
mariadb      name=mariadb                              name=mariadb   10.95.241.159   3306/TCP
smf          name=smf                                  name=smf       10.95.243.4     80/TCP
                                                                      104.155.72.30   
```

After that, you only need to get the IP from your smf service:

```bash
$ kubectl describe service smf
Name:                   smf
Namespace:              default
Labels:                 name=smf
Selector:               name=smf
Type:                   LoadBalancer
IP:                     10.95.243.4
LoadBalancer Ingress:   104.155.72.30
Port:                   <unnamed>       80/TCP
NodePort:               <unnamed>       32152/TCP
Endpoints:              10.92.2.3:80
Session Affinity:       None
No events.
```

The IP written on the field "LoadBalancer Ingress" is the one that you will
use to access your app. In this example, we will just set a static name on
our /etc/hosts file. Open the file and add it at the end:

```
104.155.72.30      smf.example.com
```

After that, go visit [http://smf.example.com/](http://smf.example.com/) to
access your new SMF forum.

# Some thoughts about the deployment

## Sharing volumes
Unfortunately, to use the PHP-FPM and Apache bitnami containers, we must share
the /app folder between these two containers. If we do so, we lost the capacity
to scale and replicate the service, because we can't use more than one writer on
a volume using a gce disk.

That means that right now this deployment can't use the replication and
scalability features of Kubernetes. 

In order to use this features, we would need to do the following:

1. Remove SMF container, and use apache container with mod_php.
It will be great if you could just avoid the usage of the PHP-FPM container.
Using it means that you MUST share your application code between the apache
and the PHP-FPM container. Maybe, from a scalability point of view, will be
better to just use an Apache container with PHP support via mod_php. Doing so,
we could just distribute the SMF app code on the container and start apache
right away. You won't need to share the code with a mountable volume because
each apache container will include its own code, and they will all have write
access to its own copy of the code.

2. Fix attachments partitioning
If you try to do what 1) suggests, you would have a new problem: attachments.
SMF uploads the attachments on a directory of the app, so if you have more than
one instance of apache in place (with mod_php) and a load balancer in front of
them, you will probably end up with the attachments partitioned between apache
containers. Two ideas come to my mind to fix this situation: developing a SMF
plugin to upload the attachments to S3 or similar or trying to use a NFS volume
on the attachments directory.

## Apache container htdocs.defaults
I've seen that the bitnami apache container includes a "htdocs.defaults"
directory that copies its contents when the container is started if the /app
folder is empty. I could have used that to distribute the SMF code and avoid
the use of a customized version of PHP-FPM, but I've used the PHP-FPM container
instead for two reasons:

1. The PHP-FPM container is now the only container writing to /app
Doing this, the Apache container mainly reads the contents from /app and serves
the static assets, but it doesn't write on it at all.

2. Database population
I needed something to populate the database on the first run, and I couldn't get
the Apache container to do that without including extra dependencies. Instead,
the PHP-SMF allowed me to write a small PHP script that does just that.