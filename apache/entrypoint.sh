#!/bin/bash
set -e
source $BITNAMI_PREFIX/bitnami-utils.sh

print_welcome_page
generate_conf_files

if [ ! "$(ls -A /app)" ]; then
  cp -r $BITNAMI_APP_DIR/htdocs.defaults/* $BITNAMI_APP_DIR/htdocs
fi

# Remove zombie pidfile
rm -f $BITNAMI_APP_DIR/logs/httpd.pid

# Copy the vhosts file on the vhosts folder
cp /bitnami/apache/smf.conf /bitnami/apache/conf/vhosts/smf.conf
sed -i -e "s/VHOSTHOSTNAME/${VHOST_HOSTNAME}/gI" /bitnami/apache/conf/vhosts/smf.conf
sed -i -e "s/PHPFPMSERVER/${PHP_FPM_CONTAINER_NAME}/gI" /bitnami/apache/conf/vhosts/smf.conf

if [[ "$@" = 'httpd' ]]; then
  exec $@ -DFOREGROUND -f $BITNAMI_APP_DIR/conf/httpd.conf
else
  exec "$@"
fi